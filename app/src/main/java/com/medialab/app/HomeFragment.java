package com.medialab.app;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.medialab.app.di.component.FragmentComponent;
import com.medialab.app.specificapp.SpecificAppFragment;

import butterknife.BindView;

/**
 * Created by Elvedin Selimoski on 6/25/18. mail: elveselimoski@gmail.com
 */
public class HomeFragment extends SpecificAppFragment {

    private static final String TAG = "HomeFragment";

    @BindView(R.id.textView)
    TextView textView;

    @Override
    protected void initFragment(View contentView) {
        super.initFragment(contentView);
        ((FragmentComponent)getFragmentComponent()).inject(this);
        setToolbarTitle("Home");
        textView.setText("Elvedin Selimoski");
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_home;
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();

        Bundle bundle = fragment.getArguments();
        if (bundle == null)
            bundle = new Bundle();

        fragment.setArguments(bundle);
        return fragment;
    }
}
