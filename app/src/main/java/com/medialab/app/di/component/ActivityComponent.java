package com.medialab.app.di.component;

import com.medialab.app.di.module.ActivityModule;
import com.medialab.app.main.MainActivity;
import com.medialab.core.di.component.ActivityComponentBase;
import com.medialab.core.di.scope.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface ActivityComponent extends ActivityComponentBase {
    void inject(MainActivity mainActivity);
}
