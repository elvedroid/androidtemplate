package com.medialab.app.di.component;

import com.medialab.app.di.module.ApplicationModule;
import com.medialab.core.di.component.ApplicationComponentBase;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent extends ApplicationComponentBase {
}
