package com.medialab.app.di.component;

import com.medialab.app.HomeFragment;
import com.medialab.app.di.module.FragmentModule;
import com.medialab.core.di.component.FragmentComponentBase;
import com.medialab.core.di.scope.PerFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = {ActivityComponent.class}, modules = {FragmentModule.class})
public interface FragmentComponent extends FragmentComponentBase {
    void inject(HomeFragment homeFragment);
}
