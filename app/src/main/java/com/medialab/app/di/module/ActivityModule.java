package com.medialab.app.di.module;

import android.support.v7.app.AppCompatActivity;

import com.medialab.core.di.module.ActivityModuleBase;

import dagger.Module;

@Module
public class ActivityModule extends ActivityModuleBase {
    public ActivityModule(AppCompatActivity activity) {
        super(activity);
    }
}
