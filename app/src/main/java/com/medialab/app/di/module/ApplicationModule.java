package com.medialab.app.di.module;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.medialab.core.di.module.ApplicationModuleBase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule extends ApplicationModuleBase {

    public ApplicationModule(Application mApplication, String baseUrl, boolean shouldProvideMockObjects) {
        super(mApplication, baseUrl, shouldProvideMockObjects);
    }

    @Provides
    @Singleton
    Resources provideResources(Context context) {
        return context.getResources();
    }
}
