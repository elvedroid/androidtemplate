package com.medialab.app.di.module;

import android.support.v4.app.Fragment;

import com.medialab.core.di.module.FragmentModuleBase;

import dagger.Module;

@Module
public class FragmentModule extends FragmentModuleBase {
    public FragmentModule(Fragment fragment) {
        super(fragment);
    }
}
