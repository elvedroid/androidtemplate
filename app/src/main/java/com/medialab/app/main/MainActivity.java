package com.medialab.app.main;

import android.os.Bundle;

import com.medialab.app.HomeFragment;
import com.medialab.app.R;
import com.medialab.app.di.component.ActivityComponent;
import com.medialab.app.specificapp.SpecificAppActivity;
import com.medialab.core.utils.Fragments;

import javax.inject.Inject;

public class MainActivity extends SpecificAppActivity {

    @Inject
    public Fragments fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((ActivityComponent)getActivityComponent()).inject(this);

        fragments.replace(R.id.container, HomeFragment.newInstance());
    }

}
