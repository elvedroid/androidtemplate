package com.medialab.app.main;

import com.medialab.app.R;
import com.medialab.app.di.component.DaggerApplicationComponent;
import com.medialab.app.di.module.ApplicationModule;
import com.medialab.core.base.BaseApplication;
import com.medialab.core.di.module.ApplicationModuleBase;

public class MainApplication extends BaseApplication {
    @Override
    public void initializeInjector(boolean mockApp) {

        applicationComponentBase = DaggerApplicationComponent
                .builder()
                .applicationModule((ApplicationModule) getApplicationModule(mockApp))
                .build();
    }

    @Override
    public ApplicationModuleBase getApplicationModule(boolean mockApp) {
        return new ApplicationModule(this, getString(R.string.base_url), mockApp);
    }
}
