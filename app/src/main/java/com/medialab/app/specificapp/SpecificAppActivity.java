package com.medialab.app.specificapp;

import com.medialab.app.di.component.ApplicationComponent;
import com.medialab.app.di.module.ActivityModule;
import com.medialab.app.di.component.DaggerActivityComponent;
import com.medialab.core.base.toolbar.ToolbarActivity;

public abstract class SpecificAppActivity extends ToolbarActivity {
    @Override
    protected void initializeInjector() {
        ActivityModule activityModule = new ActivityModule(this);
        activityComponent = DaggerActivityComponent.builder()
                .applicationComponent((ApplicationComponent) getApplicationComponent())
                .activityModule(activityModule)
                .build();
    }
}
