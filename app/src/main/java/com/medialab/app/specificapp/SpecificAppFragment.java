package com.medialab.app.specificapp;

import android.util.Log;

import com.medialab.app.di.component.ActivityComponent;
import com.medialab.app.di.module.FragmentModule;
import com.medialab.app.main.MainActivity;
import com.medialab.app.di.component.DaggerFragmentComponent;
import com.medialab.core.base.BaseActivity;
import com.medialab.core.base.toolbar.ToolbarFragment;

/**
 * Created by Elvedin Selimoski on 10/05/18. mail: elveselimoski@gmail.com
 * Rename this fragment with the app name
 * Also if you want to use ActionBar instead of Toolbar, extend BaseFragment instead of ToolbarFragment
 */

public abstract class SpecificAppFragment extends ToolbarFragment {
    private static final String TAG = "SpecificAppFragment";

    @Override
    protected void initInjector() {
        BaseActivity activity = (MainActivity) getActivity();
        if (activity == null) {
            Log.e(TAG, "Activity not found!");
            return;
        }
        try {
            FragmentModule fragmentModule = new FragmentModule(this);
            fragmentComponent = DaggerFragmentComponent.builder()
                    .activityComponent((ActivityComponent) activity.getActivityComponent())
                    .fragmentModule(fragmentModule)
                    .build();
        } catch (ClassCastException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }
}
