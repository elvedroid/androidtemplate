package com.medialab.core.base;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.medialab.core.di.component.ActivityComponentBase;
import com.medialab.core.di.component.ApplicationComponentBase;

/**
 * Created by Elvedin Selimoski on 6/25/18. mail: elveselimoski@gmail.com
 */
public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";

    protected ActivityComponentBase activityComponent;

    @Override
    public void setContentView(int layoutResID) {
        View view = getLayoutInflater().inflate(layoutResID, null);
        super.setContentView(view);
        initializeInjector();
    }

    protected abstract void initializeInjector();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public ActivityComponentBase getActivityComponent() {
        return activityComponent;
    }

    protected ApplicationComponentBase getApplicationComponent() {
        try {
            return ((BaseApplication) getApplication()).getApplicationComponentBase();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }

}
