package com.medialab.core.base;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.medialab.core.di.component.ApplicationComponentBase;
import com.medialab.core.di.module.ApplicationModuleBase;

/**
 * Created by Elvedin Selimoski on 6/25/18. mail: elveselimoski@gmail.com
 */
public abstract class BaseApplication extends MultiDexApplication {

    protected ApplicationComponentBase applicationComponentBase;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector(false);
    }

    public abstract void initializeInjector(boolean mockApp);

    public abstract ApplicationModuleBase getApplicationModule(boolean mockApp);

    public void setUpMockMode() {
        initializeInjector(true);
    }

    public ApplicationComponentBase getApplicationComponentBase() {
        return applicationComponentBase;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
