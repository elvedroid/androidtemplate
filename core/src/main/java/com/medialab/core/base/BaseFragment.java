package com.medialab.core.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medialab.core.di.component.FragmentComponentBase;

import butterknife.ButterKnife;

/**
 * Created by Elvedin Selimoski on 6/25/18. mail: elveselimoski@gmail.com
 */
public abstract class BaseFragment extends Fragment {

    private static final String TAG = "BaseFragment";

    protected View contentView;
    protected FragmentComponentBase fragmentComponent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (contentView == null) {
            contentView = inflater.inflate(getLayoutResId(), container, false);
        }

        try {
            initFragment(contentView);
        } catch (Throwable e) {
            Log.e(TAG, e.getMessage());
            if (getActivity() != null)
                getActivity().finish();
        }

        return contentView;
    }

    protected abstract int getLayoutResId();

    @CallSuper
    protected void initFragment(View contentView) {
        initInjector();
        ButterKnife.bind(this, contentView);
    }

    protected abstract void initInjector();

    public FragmentComponentBase getFragmentComponent() {
        return fragmentComponent;
    }

}
