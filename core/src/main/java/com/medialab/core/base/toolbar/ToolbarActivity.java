package com.medialab.core.base.toolbar;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.medialab.core.R;
import com.medialab.core.base.BaseActivity;

/**
 * Created by Elvedin Selimoski on 6/25/18. mail: elveselimoski@gmail.com
 */
public abstract class ToolbarActivity extends BaseActivity {

    private static final String TAG = "ToolbarActivity";

    protected Toolbar toolbar;
    protected TextView toolbarTitle;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        configureToolbar();
    }

    protected void configureToolbar() {
        if (toolbar == null) {
            toolbar = findViewById(R.id.toolbar);
            toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        }
        setSupportActionBar(toolbar);
    }

    public void hideHomeButton() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) {
            Log.e(TAG, "SupportActionBar not found!");
            return;
        }
        actionBar.setHomeAsUpIndicator(null);
    }

    public void setToolbarTitle(String title) {
        if (toolbarTitle == null) {
            Log.e(TAG, "toolbar_title not found!");
            return;
        }

        toolbarTitle.setText(title);
    }

    public void setNavigationIcon(int drawable) {
        if (toolbar == null) {
            Log.e(TAG, "Toolbar not found!");
            return;
        }

        toolbar.setNavigationIcon(drawable);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
