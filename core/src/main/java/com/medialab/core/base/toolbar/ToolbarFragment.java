package com.medialab.core.base.toolbar;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.medialab.core.base.BaseFragment;

public abstract class ToolbarFragment extends BaseFragment {

    private static final String TAG = "ToolbarFragment";
    private ToolbarActivity activity;

    @Override
    protected void initFragment(View contentView) {
        super.initFragment(contentView);
        activity  = (ToolbarActivity) getActivity();
    }

    protected void setToolbarTitle(String toolbarTitle) {
        activity.setToolbarTitle(toolbarTitle);
    }

    protected void setNavigationIcon(int icon) {
        activity.setNavigationIcon(icon);
    }

    protected Toolbar getToolbar() {
        return activity.getToolbar();
    }

    protected void hideHomeButton() {
        activity.hideHomeButton();
    }

    @Override
    protected abstract void initInjector();

    @Override
    protected abstract int getLayoutResId();


}
