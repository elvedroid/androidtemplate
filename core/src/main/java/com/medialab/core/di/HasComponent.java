package com.medialab.core.di;

/**
 * Created by Elvedin Selimoski on 12/5/17. mail: elveselimoski@gmail.com
 */

public interface HasComponent<C> {
    C getComponent();
}

