package com.medialab.core.di.component;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;


import com.medialab.core.di.module.ActivityModuleBase;
import com.medialab.core.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by Elvedin Selimoski on 12/6/17. mail: elveselimoski@gmail.com
 */

@PerActivity
@Component(dependencies = ApplicationComponentBase.class, modules = {ActivityModuleBase.class})
public interface ActivityComponentBase {

    Context context();

    AppCompatActivity activity();

}
