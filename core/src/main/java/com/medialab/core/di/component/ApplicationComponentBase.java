package com.medialab.core.di.component;

import android.content.Context;

import com.google.gson.Gson;
import com.medialab.core.di.module.ApplicationModuleBase;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Elvedin Selimoski on 12/6/17. mail: elveselimoski@gmail.com
 */

@Singleton
@Component(modules = {ApplicationModuleBase.class})
public interface ApplicationComponentBase {

    Context context();

    Gson gson();
}
