package com.medialab.core.di.component;

import com.medialab.core.di.module.FragmentModuleBase;
import com.medialab.core.di.scope.PerFragment;

import dagger.Component;

/**
 * Created by Elvedin Selimoski on 12/6/17. mail: elveselimoski@gmail.com
 */
@PerFragment
@Component(dependencies = {ActivityComponentBase.class}, modules = {FragmentModuleBase.class})
public interface FragmentComponentBase extends ActivityComponentBase {

}
