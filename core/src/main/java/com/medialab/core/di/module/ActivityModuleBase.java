package com.medialab.core.di.module;

import android.support.v7.app.AppCompatActivity;

import com.medialab.core.di.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Elvedin Selimoski on 12/6/17. mail: elveselimoski@gmail.com
 */

@Module
public class ActivityModuleBase {

    private final AppCompatActivity activity;

    public ActivityModuleBase(AppCompatActivity activity) {
        this.activity = activity;
    }

    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides
    @PerActivity
    protected AppCompatActivity activity() {
        return this.activity;
    }
}
