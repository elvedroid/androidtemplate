package com.medialab.core.di.module;

import android.support.v4.app.Fragment;

import com.medialab.core.di.scope.PerFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Elvedin Selimoski on 12/6/17. mail: elveselimoski@gmail.com
 */

@Module
public class FragmentModuleBase {
  private final Fragment fragment;

  public FragmentModuleBase(Fragment fragment) {
    this.fragment = fragment;
  }

  @Provides
  @PerFragment
  Fragment fragment() {
    return this.fragment;
  }
}
